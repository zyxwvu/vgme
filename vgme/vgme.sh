#!/bin/sh

# -----------------------------------------------------------------------------
# --- vgme.sh v.0.1.0-14                        (Michael Ehmann) 19.07.2017 ---
# ---                                                                       ---
# --- NAME:     (V*okabel und G*rammatik-Trainer von M*ichael E*hmann)      ---                                                                      ---
# --- FUNKTION: Vokabeltrainer nach dem Karteikarten-Prinzip                ---
# --- AUTOR:    MichaelEhmann                                               ---
# --- KONTAKT:  MichaelEhmann@gmx.de                                        ---
# -----------------------------------------------------------------------------



# --- Verzeichnisse und Variablen ---------------------------------------------
# >>> -------------------------------------------------------------------------
# dir_base="$(dirname $0)"          # Verzeichnis, in dem das Skript liegt
# dir_tmp="${dir_base}/tmp"         # Verzeichnis für temporäre Dateien
# dir_dtb="${dir_base}/database"    # Verzeichnis für Datenbank
# dir_audio="${dir_base}/audio"     # Verzeichnis für Aussprache
# dir_audio2="${dir_base}/audio"    # lokales Verzeichnis für Aussprache
# dir_modules="${dir_base}/modules" # Verzeichnis in dem die Module liegen
# dir_expl="${dir_base}/examples"   # Beispiele (globales Verzeichnis)
# dir_expl2="${dir_base}/examples"  # Beispiele
# database=${dir_dtb}/vgme.db       # Datenbank

dir_base="/usr/bin"               # Verzeichnis, in dem das Skript liegt
dir_tmp="/tmp/vgme_$(whoami)"     # Verzeichnis für temporäre Dateien
dir_dtb="$HOME/.vgme/database"    # Verzeichnis für Datenbank
dir_audio="/usr/share/vgme/audio" # Verzeichnis für Aussprache
dir_audio2="$HOME/.vgme/audio"    # lokales Verzeichnis für Aussprache
dir_modules="/usr/lib/vgme"       # Verzeichnis in dem die Module liegen
dir_expl="/usr/share/doc/vgme"    # Beispiele (globales Verzeichnis)
dir_expl2="$HOME/.vgme/examples"  # Beispiele
database=${dir_dtb}/vgme.db       # Datenbank

lines_min=20                      # minimale Zeilenzahl für das Programm
cols_min=72                       # minimale Spaltenzahl für das Programm
if [ ! -d "${dir_tmp}"   ]; then mkdir -p "${dir_tmp}"; fi
if [ ! -d "${dir_expl2}" ]; then
    mkdir -p "${dir_expl2}"
    cp -r ${dir_expl}/examples/* ${dir_expl2}/
fi
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Module ------------------------------------------------------------------
# >>> -------------------------------------------------------------------------
color=true
lang=de
. ${dir_modules}/OUTPUT ${lang} ${color} # Programmausgaben (deutsch, Farbe an)
. ${dir_modules}/STATISTIC       # Statistiken
# . ${dir_modules}/SIZE            # prüfe Terminalgröße
. ${dir_modules}/TIDYUP          # prüfe Terminalgröße
. ${dir_modules}/HELP            # Hilfedateien
. ${dir_modules}/EDIT            # Editieren von Tabellen
. ${dir_modules}/ORDER           # Sortierung der Vokabeln
. ${dir_modules}/LEARN           # Lernen der Vokabeln
. ${dir_modules}/FIND            # Suchen nach Vokabeln
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- prüfe Terminalgröße und fange Strg+C ab ---------------------------------
# >>> -------------------------------------------------------------------------
# SIZE "${lines_min}" "${cols_min}"
trap '' 2 >/dev/null             # fängt Strg+C ab, Programm mit :q beenden!
if [ $(pgrep vgme | wc -l) -gt 2 ]; then
    process=active
else
    process=inactive
fi
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Überprüfen der Eingabeparamter und entsprechendes Handeln ---------------
# >>> -------------------------------------------------------------------------
while getopts ':E:L:O:F:S:P:h' OPTION; do # EDIT, LEARN, ORDER, FIND, HELP
    case $OPTION in
        E)  opt_E=$OPTARG
            if [ "${process}" == "active" ] && [ ! "${opt_E}"=="h" ]; then
                printf "${err} ${vgme_err_active}\n"
                exit 1
            fi
            if [ "${opt_E}" == "h" ]; then
                mode_help=true
                help E ${dir_modules} ${color} ${lang}
            fi
        ;;
        L)  if [ "${process}" == "active" ] && [ ! "${opt_L}"=="h" ]; then
                printf "${err} ${vgme_err_active}\n"
                exit 1
            fi

            if [ ${OPTARG:0:1} == "m" ]; then
                mute=true
                OPTARG=${OPTARG:1:${#OPTARG}}
            fi

            if echo $@ | grep "\-L .*" > /dev/null; then
                opt_L=NN
            else
                opt_L=$OPTARG
            fi
            if [ "${opt_L}" == "h" ]; then
                mode_help=true
                help L ${dir_modules} ${color} ${lang}
            fi
        ;;
        O)  if [ "${process}" == "active" ] && [ ! "${opt_O}"=="h" ]; then
                printf "${err} ${vgme_err_active}\n"
                exit 1
            fi
            opt_O=$OPTARG
            if [ "${opt_O}" == "h" ]; then
                mode_help=true
                help O ${dir_modules} ${color} ${lang}
            fi
        ;;
        F)  if echo $@ | grep "\-F .*" > /dev/null; then
                opt_F="w"
            else
                opt_F=$OPTARG
            fi
            if [ "${opt_F}" == "h" ]; then
                mode_help=true
                help F ${dir_modules} ${color} ${lang}
            fi
        ;;
        S)  opt_S=$OPTARG
            if [ "${opt_S}" == "h" ]; then
                mode_help=true
                help S ${dir_modules} ${color} ${lang}
            fi
        ;;
        P)  opt_P=$OPTARG
            if [ "${opt_P}" == "h" ]; then
                mode_help=true
                help P ${dir_modules} ${color} ${lang}
            fi
        ;;
        h)  mode_help=true
            help H ${dir_modules} ${color} ${lang}
        ;;
        *)  if echo $@ | grep "\-L$"   > /dev/null; then
                opt_L=ALL
            else
                printf "${err} ${vgme_err_opt}\n"; exit 1
            fi
        ;;
    esac
done

if echo "$@" | grep ":h" > /dev/null; then
    help : ${dir_modules} ${color} ${lang}
    exit 0
fi


if [ "${mode_help}" == "true" ]; then exit 0; fi

if [ ! -e "${database}" ] && [ ! ${opt_E} ]; then
    printf "${err} ${vgme_err_nodb}\n"
    exit 1
fi


if [ "${opt_L}" ] && [ ! "${opt_O}" ]; then
    opt_O=rs0
    arg="+1"
fi


if [ "${opt_O}" ] && [ ! "${opt_L}" ]; then
    opt_L=ALL
fi

options=$@
if [ ! "${options}" ]; then
    opt_O=rs0
    arg="+1"
    opt_L=ALL
fi

if [ $opt_P ]; then
   speak_word="${@:3:$#}"
   speak "${dir_audio}"  \
         "${dir_audio2}" \
         "${@:2:1}"      \
         "${speak_word}"
exit 0
fi

if [ $opt_E ]; then
    case $opt_E in
        i)   EDITinstall  "${database}" "${dir_tmp}" "${dir_modules}" ${@:2:$#}
        ;;
        a)   EDITadd      "${database}" "${dir_tmp}" "${dir_modules}" "false" \
                          "false" ${@:2:$#}
        ;;
        u)   EDITadd      "${database}" "${dir_tmp}" "${dir_modules}" "false" \
                          "true" ${@:2:$#}
        ;;
        r|d) EDITdelete   "${database}" "${dir_tmp}" "${dir_modules}" "false" \
                           ${@:2:$#}
        ;;
        m)   EDITmodifyaa "${database}" "${dir_tmp}" "${dir_modules}" "${2}"  \
                          "${@:3:$#}"   "true"       "false"
        ;;
        *) printf "${err} ${vgme_err_E}\n"; exit 1
        ;;
    esac
fi


if [ $opt_S ]; then
    case $opt_S in
        c)   STATSdate2classic "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
        ;;
        l)   STATSlevel        "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
        ;;
        cl)  if echo "${@:3:$#}" | grep "-" > /dev/null; then
                 printf "${err} ${STATS_err_date}\n"
                 exit 1
             fi
             STATSdate2classic "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
             printf "\n"
             STATSlevel        "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
        ;;
        lc)  if echo "${@:3:$#}" | grep "-" > /dev/null; then
                 printf "${err} ${STATS_err_date}\n"
                 exit 1
             fi
             STATSlevel        "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
             printf "\n"
             STATSdate2classic "${dir_tmp}" "${database}" "${2}" "${@:3:$#}"
        ;;
        m)   STATSmistake      "${dir_tmp}" "${database}" "${@:2:$#}"
        ;;
        *) printf "${err} ${vgme_err_S}\n"; exit 1
        ;;
    esac
fi


if [ $opt_O ]; then
    case ${opt_O: -2:2} in
        s0) split=s0
        ;;
        s1) split=s1
        ;;
        s2) split=s2
        ;;
    esac
    case ${opt_O/s[0-2]/} in
        r)      check_opt_O "r${split}"
        ;;
        l)      check_opt_O "l${split}"
        ;;
        lr|rl)  check_opt_O "lr${split}"
        ;;
        d)      check_opt_O "d${split}"
        ;;
        dr|rd)  check_opt_O "dr${split}"
        ;;
        p)      check_opt_O "p${split}"
        ;;
        pr|rp)  check_opt_O "pr${split}"
        ;;
        *)      printf "${err} ${vgme_err_O}\n"; exit 1
        ;;
    esac
fi


if [ $opt_L ]; then
    if [ "$opt_L" == "ALL" ]; then
        check_opt_L "NN"
        units=${@:2:$#}
        LEARN "${opt_L}"   "${opt_O}" "${split}" "${database}"                \
              "${dir_tmp}"
    else
        case ${opt_L:0:2} in
            "<="|">="|"=="|"NN")
            if [ "${arg}" ]; then
                check_opt_L "${opt_L}"
                units=${@:2:$#}
                LEARN "${opt_L}"   "${opt_O}" "${split}" "${database}"        \
                      "${dir_tmp}" "${units}"
            else
                check_opt_L "${opt_L}"
                units=${@:3:$#}
                LEARN "${opt_L}"   "${opt_O}" "${split}" "${database}"        \
                      "${dir_tmp}" "${units}"
            fi
            ;;
            *)      printf "${err} ${vgme_err_L}\n"; exit 1
            ;;
        esac
    fi
fi


if [ $opt_F ]; then
    case $opt_F in
        w)      find2table ${dir_tmp} ${database} "w" "${2}" "${@:3:$#}"
        ;;
        i)      find2table ${dir_tmp} ${database} "i" "${2}" "${@:3:$#}"
        ;;
        l)      find2table ${dir_tmp} ${database} "l" "${2}" "${@:3:$#}"
        ;;
        c)      find2table ${dir_tmp} ${database} "c" "${2}" "${@:3:$#}"
        ;;
        *)      printf "${err} ${vgme_err_F}\n"; exit 1
        ;;
    esac
fi
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- räume auf ---------------------------------------------------------------
# >>> -------------------------------------------------------------------------
if [ $opt_L ]; then
    TIDYUP "${database}" "${dir_tmp}"
fi
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------

#!/bin/s
# -----------------------------------------------------------------------------
# --- EDIT                                                                  ---
# ---                                                                       ---
# --- Editieren der Vokabel-Tabellen.                                       ---
# -----------------------------------------------------------------------------

# +--------------+----------------------------------------------+-------------+
# | Funktionen:  | Effekt:                                      | Option:     |
# +--------------+----------------------------------------------+-------------+
# | gentxt       | generiere .txt-Dateien ...                   | -           |
# | ask          | Nachfrage                                    | -           |
# | check_table  | überprüfe, ob Datenbank/Tabelle existiert    | -           |
# | check_tname  | überprüfe Namen von Vokabellisten            | -           |
# | check_tfiles | überprüfe Vokabellisten                      | -           |
# | check_input  | überprüfe Eingaben für EDIT-Funktionen       | -           |
# | print_txt    | generiere Kurzvorschau von Vokabeln          | -           |
# | update_table | Zusammenführen von Vokabeln                  | -           |
# | EDITinstall  | Hinzufügen/Überschreiben von Vokabeltabellen | (-Ei)       |
# | EDITadd      | Ergänzen von Vokabeln in Listen              | (-Ea)       |
# | EDITdelete   | Löschung von Vokabeln oder ganzen Tabellen   | (-Ed = -Er) |
# | EDITmodifyaa | modifizieren bestehender Datenbankeinträge   | (-Em;   :m) |
# +--------------+----------------------------------------------+-------------+

# ########################################################################### #
# ### benötigt Funktionen des Moduls OUTPUT                               ### #
# ### zum Testen der Funktionen '. ./OUTPUT && . ./EDIT' ausführen.       ### #
# ########################################################################### #



# --- generiere .txt-Dateien (lösche Leerzeilen, Leerzeichen, sortiere) -------
# >>> -------------------------------------------------------------------------
gentxt () {
  cat $@ | sed "/^\ *$/d;s/@/\t/g;s/\ *\t\ */\t/g" | sort | uniq
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Nachfrage ---------------------------------------------------------------
# -----------------------------------------------------------------------------
function ask () {
    local str=$1

    read -p "${str} [J/n] " AN
    case ${AN} in
        Yes|yes|Y|y|Ja|ja|J|j|"")
        ;;
        *) exit 1
        ;;
    esac
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# --- überprüfe, ob Datenbank/Tabelle existiert -------------------------------
# >>> -------------------------------------------------------------------------
function check_table () {
    database=$1                  # Datenbank
    tocheck=${@:2:$#}            # zu überprüfende Tabelle(n)
    # Alle weiteren verwendeten Variablen werden im Modul OUTPUT definiert.

    if [ ! -f ${database} ]      # Datenbank existiert nicht
        then
        printf "${err} ${EDIT_err_nodb}"
        printf "${clr_bold}${database}${clr_normal}\n"
        return 1
    else                         # prüfe, ob Tabelle in Datenbank enthalten ist
        tables=$(sqlite3 ${database} -noheader ".tables" |                    \
                 sed "s/statistic_unit//g;s/statistic_word//g")
        for i in ${tocheck}; do
            if ! echo ${tables} | grep -E "${i}\ |${i}$" > /dev/null
                then
                printf "${err} ${EDIT_err_notable}"
                printf "${clr_bold}${i}${clr_normal}\n\n"
                printf "${EDIT_hint_tables} ($(echo "${tables}" | wc -w)):\n"
                printf "${clr_bold}${tables}${clr_normal}\n"
                return 1
            fi
        done
    fi

    return 0
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- überprüfe Namen von Vokabellisten ---------------------------------------
# >>> -------------------------------------------------------------------------
function check_tname () {
    txt_files=$@                 # beliebig viele .txt-Dateien
    # Alle weiteren verwendeten Variablen werden im Modul OUTPUT definiert.

    for i in ${txt_files}; do
        i=$(basename $i)
        if [[ ! "${i:0:2}" == [a-z][a-z] ]] || [[ ! "${i:2:2}" == [A-Z][A-Z] ]];
            then
            printf "${err} ${EDIT_err_txtname}"
            printf "${clr_bold}${i}${clr_normal}\n"
            return 1
        fi
    done
    return 0
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- überprüfe Vokabellisten -------------------------------------------------
# >>> -------------------------------------------------------------------------
function check_tfiles () {
    txt_files=$@
    # Alle weiteren verwendeten Variablen werden im Modul OUTPUT definiert.

    if [ ! "$1" ]
        then
        printf "${err} ${EDIT_err_nofile}\n"
        return 1
    fi

    for i in ${txt_files}; do
        if [ ! -f ${i} ];        # prüfe, ob Datei existiert
            then
            printf "${err} ${EDIT_err_notxt}"
            printf "${clr_bold}${i}${clr_normal}\n"
            return 1
        fi

        if [ ! "${i##*.}" == "txt" ];
            then
            printf "${err} ${EDIT_err_txtext}"
            printf "${clr_bold}${i}${clr_normal}\n"
            return 1
        fi

        if echo $(basename ${i%.txt}) | grep "\." > /dev/null;
            then
            printf "${err} ${EDIT_err_txtdot}"
            printf "${clr_bold}${i}${clr_normal}\n"
            return 1
        fi
        # Spaltenanzahl muss mindestens 2 und darf höchstens 4 sein.
        minmax=$(cat ${i}                                       | \
                 sed "s/@/\t/g"                                 | \
                 awk -F"\t" '{if (NF < 2 || NF > 4) print NR}'  | \
                 sed -ne "1p")
        if [ "${minmax}" ]
            then
            printf "${err} ${EDIT_err_txtcol}"
            printf "${clr_bold}${i} (${EDIT_line}: ${minmax})${clr_normal}\n"
            return 1
        fi
    done
    return 0
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- überprüfe Eingaben für EDIT-Funktionen ----------------------------------
# >>> -------------------------------------------------------------------------
function check_input () {
    database=$1                  # Datenbank
    arg1=$2                      # erstes Eingabeargument nach Datenbank
    argr=${@:3:$#}               # restliche Eingabeargumente

    # beende, falls erste Eingabe keine Tabelle ist
    if ! check_table ${database} ${arg1}; then exit 1; fi

    # überprüfe, ob die Eingabe ein HERE-Dokument/String ist
    if [ ! -t 0 ]
        then input="here"

    # überprüfe, ob alle Argumente Tabellen sind
    elif check_table ${database} ${argr} > /dev/null;
        then input="table"

    # überprüfe, ob die restlichen Argumente geeignete .txt-Dateien sind
    elif check_tfiles ${argr};
        then input="txt"

    # beende, falls keine passende Eingabe gefunden
    else
        exit 1
    fi
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- generiere eine Kurzvorschau von Vokabeln einer .txt-Datei ---------------
# >>> -------------------------------------------------------------------------
function print_txt () {
    file=$1                      # Datei mit darzustellendem Inhalt
    str=$2                       # ein String
    dir_tmp=$3                   # Verzeichnis für temporäre Dateien
    # Alle weiteren verwendeten Variablen werden im Modul OUTPUT definiert.

    # generiere Datei mit anzuzeigenden Vokabeln:
    gentxt ${file} > ${dir_tmp}/EDIT_table_add1

    total=$(cat ${dir_tmp}/EDIT_table_add1 | wc -l)

    # Länges des längstens Wortes in den ersten 15 Zeilen in Spalte 1 bzw. 2
    len1=$(sed -ne "1,15p" ${dir_tmp}/EDIT_table_add1 | \
    awk -F"\t" '{ print length($1) }' | sort -nr | sed -ne "1p")
    len2=$(sed -ne "1,15p" ${dir_tmp}/EDIT_table_add1 | \
    awk -F"\t" '{ print length($2) }' | sort -nr | sed -ne "1p")

    # falls die Gesamtlänge der Worte länger ist, als die Konsole kürze
    # geschickt ab.
    if [ $(( $len1 + $len2 + 1 )) -gt $(tput cols) ]
    then
        if   [ $len1 -gt $(( $(tput cols) / 2 - 1 )) ] && \
             [ $len2 -le $(( $(tput cols) / 2     )) ]
        then
            a=$(( $(tput cols) - $len2 - 1 ))
            b=$len2
        elif [ $len2 -gt $(( $(tput cols) / 2     )) ] && \
             [ $len1 -le $(( $(tput cols) / 2 - 1 )) ]
        then
            a=$len1
            b=$(( $(tput cols) - $len1 - 1 ))
        else
            a=$(( $(tput cols)/2 - 1 ))
            b=$(( $(tput cols)/2     ))
        fi
    else
        a=$len1
        b=$len2
    fi

    awk -v"a=${a}" \
        -v"b=${b}" \
        -F"\t" '{ {OFS=FS} {
            if( length($1) > a) {
                if ( length($2) > b) {
                    print substr($1,1,a-4)" ### ", substr($2,1,b-4)" ###"
                }
                else {
                    print substr($1,1,a-4)" ### ", $2
                }
            }
            else {
                if ( length($2) > b) {
                    print $1" ", substr($2,1,b-4)" ###"
                }
                else {
                    print $1" ", $2
                }
            }
        }}' ${dir_tmp}/EDIT_table_add1 | \
    sed -ne "1,15p" > ${dir_tmp}/EDIT_table_add2

   # generiere Farbdatei für Tabelle:
   printf "${clr_table_prev1}\t${clr_table_prev2}\n\t\n%.0s" {1..8} \
   > ${dir_tmp}/EDIT_table_add_clr

   # Ausgabe der Kurzvorschau:
   printf "\n${EDIT_voc} (${total}) ${str}\n"
   table ${dir_tmp}/EDIT_table_add2 ${dir_tmp}/EDIT_table_add_clr
   if [ ${total} -gt 15 ]
       then printf "($(( ${total} -15 )) ${EDIT_more})\n"
   fi
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Zusammenführen von Vokabeln ---------------------------------------------
# >>> -------------------------------------------------------------------------
function update_table () {
    tables=$@

    # Führt Einträge, in denen aa, aa_remark, BB und BB_remark identisch sind,
    # so zusammen, daß der höchste Level etc. erhalten bleibt.
    for table in ${tables}; do
        tmp=${dir_tmp}/EDIT_${table}.csv

        # füge Daten zusammen und exportiere die Ausgabe
        sqlite3 ${dir_dtb}/vgme.db -separator @ "
            SELECT
                MAX(reference)   ,
                aa               ,
                aa_remark        ,
                aa_quantity      ,
                MAX(aa_level)    ,
                MAX(aa_highest)  ,
                MAX(aa_first)    ,
                MAX(aa_last)     ,
                MAX(aa_next)     ,
                BB               ,
                BB_remark        ,
                BB_quantity      ,
                MAX(BB_level)    ,
                MAX(BB_highest)  ,
                MAX(BB_first)    ,
                MAX(BB_last)     ,
                MAX(BB_next)
            FROM
                ${table}
            GROUP BY
                aa                ,
                BB                ,
                aa_remark         ,
                BB_remark
            ORDER BY
                reference
        " | sort -nt"@" | awk -F"@" '{{OFS=FS} {$1=NR} print $0}' > ${tmp}

        # lösche die Tabelle.
        sqlite3 ${dir_dtb}/vgme.db "DELETE FROM ${table}"

        # lese exportierte Datenbank wieder ein.
        sqlite3 ${dir_dtb}/vgme.db -separator @ ".import ${tmp} ${table}"
    done
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Hinzufügen/Überschreiben von Vokabeltabellen (-Ei) ----------------------
# >>> -------------------------------------------------------------------------
function EDITinstall () {
    database=$1                  # Datenbank
    dir_database=$(dirname $1)   # Verzeichnis der Datenbank
    dir_tmp=$2                   # Verzeichnis für temporäre Dateien
    dir_modules=$3               # Verzeichnis mit Modulen (awk-Skript)
    txt_files=${@:4:$#}          # beliebig viele .txt-Dateien

    if ! check_tname  ${txt_files}; then exit 1; fi
    if ! check_tfiles ${txt_files}; then exit 1; fi

    total=$(echo ${txt_files} | wc -w)

    # Erstelle Ordner für Datenbank, falls nicht vorhanden.
    if [ ! -d ${dir_dtb} ]; then mkdir -p ${dir_dtb}; fi

    # Ausgabe:
    for i in ${txt_files}; do
        if check_table ${database} $(echo $( basename $i) | sed "s/.txt//g")  \
           > /dev/null; then
            printf "${clr_hint}Warnung:${clr_normal} "
            printf "${clr_bold}${i}${clr_normal}\t"
            printf " existiert bereits -- Ersetze\n"
        fi
    done > ${dir_tmp}/EDIT_warning
    table  ${dir_tmp}/EDIT_warning

    for i in ${txt_files}; do units+=($(basename $i)); done
    printf "\n${EDIT_table} (${total}) "
    printf "${EDIT_hint_add}\n"
    printf "${clr_bold}${units[*]}${clr_normal}\n\n" | sed "s/.txt//g"
    ask "${EDIT_continue} "

    # Erstelle Datenbank vgme.db und Tabelle statistic_unit in Datenbank.
    sqlite3 ${database} "CREATE TABLE IF NOT EXISTS statistic_unit (
        date         INT UNSIGNED  ,
        unit         VARCHAR(128)  ,
        tuple        VARCHAR(128)  ,
        time         INT UNSIGNED  ,
        right        INT UNSIGNED  ,
        right_series INT UNSIGNED  ,
        wrong        INT UNSIGNED  ,
        wrong_series INT UNSIGNED  ,
        words_begin  INT UNSIGNED  ,
        words_total  INT UNSIGNED  ,
        words_inuse  INT UNSIGNED  ,
        progress     INT UNSIGNED  ,
        aa_levels    VARCHAR(128)  ,
        BB_levels    VARCHAR(128)
    );" || return 1

    # Erstelle Tabelle mit statistic_word in Datenbank.
    sqlite3 ${database} "CREATE TABLE IF NOT EXISTS statistic_word (
        date         INT UNSIGNED  ,
        unit         VARCHAR(128)  ,
        tuple        VARCHAR(128)  ,
        aa           VARCHAR(128)  ,
        aa_remark    VARCHAR(128)  ,
        BB           VARCHAR(128)  ,
        BB_remark    VARCHAR(128)  ,
        word         VARCHAR(128)  ,
        level        TINYINT       ,
        answer       VARCHAR(128)  ,
        success      TINYINT       ,
        delay        INT UNSIGNED
    );" || return 1

    j=1; for i in ${txt_files}; do
        # Variablendefinition.
        aaBB="$(basename ${i%.txt})"
        dir_aaBB="${dir_dtb}/${aaBB:0:4}"
        txt_file="${dir_aaBB}/$(basename $i)"
        csv_file="${dir_aaBB}/$(basename ${i%.txt}).csv"

        # Erstelle Ordner für das Sprachtupel, falls nicht vorhanden.
        if [ ! -d ${dir_aaBB} ]; then mkdir -p ${dir_aaBB}; fi

        # Diese Funktion macht die eigentliche Arbeit und wird nur für eine
        # einfache Übergabe an die respond-Funktion definiert.
        function create_table () {
            # Kopiere Original- und Datenbankdatei(en) in diesen Ordner.
            gentxt ${i} > ${txt_file} 2>/dev/null                 || return 1
            ${dir_modules}/createdb.awk ${txt_file} > ${csv_file} || return 1

            # Erstelle Tabelle aaBB in Datenbank.
            sqlite3 ${database} "CREATE TABLE IF NOT EXISTS ${aaBB} (
                reference   INTEGER           ,
                aa          VARCHAR(128)      ,
                aa_remark   VARCHAR(128)      ,
                aa_quantity TINYINT UNSIGNED  ,
                aa_level    TINYINT           ,
                aa_highest  TINYINT           ,
                aa_first    INT UNSIGNED      ,
                aa_last     INT UNSIGNED      ,
                aa_next     INT UNSIGNED      ,
                BB          VARCHAR(128)      ,
                BB_remark   VARCHAR(128)      ,
                BB_quantity TINYINT UNSIGNED  ,
                BB_level    TINYINT           ,
                BB_highest  TINYINT           ,
                BB_first    INT UNSIGNED      ,
                BB_last     INT UNSIGNED      ,
                BB_next     INT UNSIGNED
            );" || return 1

            # Löschen der Statistik, falls eine alte Tabelle überschrieben wird
            sqlite3 ${database} "
                DELETE FROM
                    statistic_word
                WHERE
                    unit='${aaBB}'
            "   || return 1
            sqlite3 ${database} "
                DELETE FROM
                    statistic_unit
                WHERE
                    unit='${aaBB}'
            "   || return 1

            # Falls die Tabelle nicht-leer ist wird sie überschrieben.
            sqlite3 ${database} "DELETE FROM ${aaBB}" || return 1
            sqlite3 ${database} -separator @ \
                ".import ${csv_file} ${aaBB}"         || return 1
        }
        respond "(${j}/${total}) Installiere $(basename ${i})" create_table
        j=$(( $j + 1 ))
    done
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Ergänzen von Vokabeln in Listen (-Ea) -----------------------------------
# >>> -------------------------------------------------------------------------
function EDITadd () {
    database=$1                  # Datenbank
    dir_database=$(dirname $1)   # Verzeichnis der Datenbank
    dir_tmp=$2                   # Verzeichnis für temporäre Dateien
    dir_modules=$3               # Verzeichnis mit Modulen (awk-Skript)
    local quiet=$4
    local activate=$5
    if [ ! ${6} ]; then printf "${err} ${EDIT_err_noarg}\n"; exit 1; fi
    arg1=$(basename ${6})        # eine sqlite3-Tabelle.
    argr=${@:7:$#}               # eine od. mehrere .txt-Dateien,
                                 # ein HERE-Dokument, oder ein HERE-String.
    local i
    # Variablendefinition.
    dir_aaBB="${dir_dtb}/${arg1:0:4}"
    txt_file="${dir_aaBB}/${arg1}.txt"
    csv_file="${dir_aaBB}/${arg1}.csv"

    if [ -t 0 ] && [ "${argr}" == "" ]
        then printf "${err} ${EDIT_err_missing}\n"
        exit 1
    fi
    check_input ${database} ${arg1} ${argr}

    # Falls HERE-Dokument übergeben, generiere .txt-Datei daraus.
    # Variable 'input' wird von check_input gesetzt.
    if [ "${input}" == "here"  ]; then
        argr="${dir_tmp}/EDIT_here.txt"
        cat - | sed "s/\\\t/\t/g" > ${argr}
        if ! check_tfiles ${argr}; then exit 1; fi
        # sorge dafür, daß der spätere read-Befehl richtig funktioniert
        t=$(tty <&1) || t=$(tty <&2) || exit 1
        exec < $t
    elif [ ! "${input}" == "txt" ]; then
        printf "${err} ${EDIT_err_txtext}"
        printf "${clr_bold}${5}${clr_normal}\n"
        exit 1
    fi

    gentxt ${argr} > ${dir_tmp}/EDIT_add


    if [ ! "${quiet}" == "true" ]; then
        # Ausgabe der Vokabelübersicht:
        if [[ "${activate}" == "true" ]]; then
           str=$(EDIT_hint_use ${clr_bold}${arg1}${clr_normal})
        else
           str=$(EDIT_hint_add ${clr_bold}${arg1}${clr_normal})
        fi
        print_txt "${dir_tmp}/EDIT_add" "${str}" "${dir_tmp}"

        echo ""
        ask "${EDIT_continue} "
    fi

    function add_to_table () {
        # erstelle modifizierte .txt- und .csv-Datei.
        gentxt ${txt_file} ${argr} > ${dir_tmp}/EDIT_txt        || return 1
        mv ${dir_tmp}/EDIT_txt ${txt_file}                      || return 1
        ${dir_modules}/createdb.awk ${txt_file} > ${csv_file}   || return 1

        # überarbeite die bestehende Datenbank.
        sqlite3 ${database} -separator @ \
            ".import ${csv_file} ${arg1}"                       || return 1
        update_table ${arg1}                                    || return 1
    }
    if [ ! "${quiet}" == "true" ]; then
        respond "(*/${total}) ${EDIT_add}" add_to_table
    else
        respond "(*/${total}) ${EDIT_add}" add_to_table > /dev/null
    fi


    ### Vokabeln direkt beim Hinzufuegen aktivieren ###########################
    ###########################################################################
    if [[ "${activate}" == "true" ]]; then

    local i=1

    while [ $i -le $(cat ${dir_tmp}/EDIT_table_add1 | wc -l) ]; do
        local aa=$(awk -F"\t" '{print $1}' ${dir_tmp}/EDIT_table_add1 |       \
                    sed "s/ (.*)$//g" | sed -n "${i}p")
        local BB=$(awk -F"\t" '{print $2}' ${dir_tmp}/EDIT_table_add1 |       \
                    sed "s/ (.*)$//g" | sed -n "${i}p")
        if awk -F"\t" '{print $1}' ${dir_tmp}/EDIT_table_add1         |       \
           sed -n "${i}p"                                             |       \
           grep ")" > /dev/null
        then
            local aa_remark=$(awk -F"\t" '{print $1}'                         \
                              ${dir_tmp}/EDIT_table_add1              |       \
                              sed -n "${i}p" | sed "s/.*(//g" | tr -d ")" )
        else
            local aa_remark=""
        fi

        if awk -F"\t" '{print $2}' ${dir_tmp}/EDIT_table_add1         |       \
           sed -n "${i}p"                                             |       \
           grep ")" > /dev/null
        then
            local BB_remark=$(awk -F"\t" '{print $2}'                         \
                              ${dir_tmp}/EDIT_table_add1              |       \
                              sed -n "${i}p" | sed "s/.*(//g" | tr -d ")" )
        else
            local BB_remark=""
        fi

        baa=$(echo ${aa} | sed "s/'/''/g")
        baa_remark=$(echo ${aa_remark} | sed "s/'/''/g")
        bBB=$(echo ${BB} | sed "s/'/''/g")
        bBB_remark=$(echo ${BB_remark} | sed "s/'/''/g")

        sqlite3 ${database} -noheader "
            UPDATE
                ${table}
            SET
                aa_level="0",
                BB_level="0"
            WHERE
                aa='${baa}'
                AND aa_remark='${baa_remark}'
                AND BB='${bBB}'
                AND BB_remark='${bBB_remark}'
            "

        i=$(( $i + 1 ))
    done

    fi
    ###########################################################################
    ###########################################################################
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# --- Löschung von Vokabeln oder ganzen Tabellen (-Ed = -Er) ------------------
# >>> -------------------------------------------------------------------------
function EDITdelete () {
    database=$1                  # Datenbank
    dir_database=$(dirname $1)   # Verzeichnis der Datenbank
    dir_tmp=$2                   # Verzeichnis für temporäre Dateien
    dir_modules=$3               # Verzeichnis mit Modulen (awk-Skript)
    local quiet=$4
    if [ ! ${5} ]; then printf "${err} ${EDIT_err_noarg}\n"; exit 1; fi
    arg1=$(basename ${5})        # eine sqlite3-Tabelle.
    argr=${@:6:$#}               # eine od. mehrere sqlite3-Tabellen,
                                 # eine od. mehrere .txt-Dateien,
                                 # ein HERE-Dokument, oder ein HERE-String.

    local i
    local aa
    local BB
    check_input ${database} ${arg1} ${argr}

    # Variablendefinition.
    dir_aaBB="${dir_dtb}/${arg1:0:4}"
    csv_file="${dir_aaBB}/${arg1}.csv"
    txt_file="${dir_aaBB}/${arg1}.txt"
    txt_diff="${dir_tmp}/EDIT_diff.txt"
    txt_delete="${dir_tmp}/EDIT_delete.txt"

    # Lösche angegebene Tabellen, falls alls Argumente Tabellen sind.
    # Variable 'input' wird von check_input gesetzt.
    if [ "${input}" == "table" ]; then
        total=$(echo ${arg1} ${argr} | wc -w)
            printf "\n${EDIT_table} (${total}) "
            printf "${EDIT_hint_del}\n"
            printf "${clr_bold}${arg1} ${argr}${clr_normal}\n\n"              \
                                     | sed "s/.txt//g"

            ask "${EDIT_condelet} "

        for i in ${@:5:$#}; do
            rm ${dir_dtb}/${i:0:4}/${i}.{csv,txt}
            sqlite3 ${database} "DROP TABLE ${i}"

            sqlite3 ${database} "
                DELETE FROM
                    statistic_word
                WHERE
                    unit='${i}'
            "
            sqlite3 ${database} "
                DELETE FROM
                    statistic_unit
                WHERE
                    unit='${i}'
            "
        done
    else
        # Falls HERE-Dokument übergeben, generiere .txt-Datei daraus.
        if [ "${input}" == "here"  ]; then
            argr="${dir_tmp}/EDIT_here.txt"
            cat - | sed "s/\\\t/\t/g" > ${argr}
            if ! check_tfiles ${argr}; then exit 1; fi
            # sorge dafür, daß der spätere read-Befehl richtig funktioniert
            t=$(tty <&1) || t=$(tty <&2) || exit 1
            exec < $t
        fi

        # .txt-Datei mit den zu entfernenden Vokabeln:
        gentxt ${argr} > ${txt_diff}

        # Ausgabe der Vokabelübersicht:
        if [ ! "${quiet}" == "true" ]; then
            str=$(EDIT_hint_del ${clr_bold}${arg1}${clr_normal})
            print_txt "${txt_diff}" "${str}" "${dir_tmp}"

            echo ""
            read -p "${EDIT_condelet} [Y/n] " AN
        else
            AN="Yes"
        fi

        case ${AN} in
          Yes|yes|Y|y|Ja|ja|J|j|"")

          function remove_from_table () {
            # erstelle csv-Datei mit den zu löschenden Zeilen:
            comm -12 ${txt_file} ${txt_diff} > ${dir_tmp}/EDIT_del || return 1
            ${dir_modules}/createdb.awk ${dir_tmp}/EDIT_del > ${txt_delete}  \
                                                                   || return 1

            # erstelle modifizierte .txt- und .csv-Datei.
            comm -23 ${txt_file} ${txt_diff} > ${dir_tmp}/EDIT_txt || return 1
            mv ${dir_tmp}/EDIT_txt ${txt_file}                     || return 1
            ${dir_modules}/createdb.awk ${txt_file} > ${csv_file}  || return 1

            # überarbeite die bestehende Datenbank.
            # ganze Tabelle vokabelweise zu Löschen dauert natürlich ...
            i=1
            while [ $i -le $(cat ${txt_delete} | wc -l) ]; do
            aa=$(cat ${txt_delete} | sed -ne "${i}p" | awk -F"@" '{print $2 }')
            ar=$(cat ${txt_delete} | sed -ne "${i}p" | awk -F"@" '{print $3 }')
            BB=$(cat ${txt_delete} | sed -ne "${i}p" | awk -F"@" '{print $10}')
            Br=$(cat ${txt_delete} | sed -ne "${i}p" | awk -F"@" '{print $11}')
                sqlite3 ${database} "
                    DELETE FROM
                        ${arg1}
                    WHERE
                        aa=\"${aa}\"
                        AND aa_remark=\"${ar}\"
                        AND BB=\"${BB}\"
                        AND BB_remark=\"${Br}\"
                "
                # sqlite3 ${database} "
                #     DELETE FROM
                #         statistic_word
                #     WHERE
                #         aa=\"${aa}\"
                #         AND aa_remark=\"${ar}\"
                #         AND BB=\"${BB}\"
                #         AND BB_remark=\"${Br}\"
                #         AND unit='${arg1}'
                # "

                i=$(( $i + 1))
            done                 || return 1
            update_table ${arg1} || return 1
          }
          if [ ! "${quiet}" == "true" ]; then
              respond "(*/${total}) ${EDIT_del}" remove_from_table
          else
              respond "(*/${total}) ${EDIT_del}" remove_from_table > /dev/null
          fi
          ;;
          *)
          echo ""
          ;;
        esac
    fi
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------



# >>> modifizieren bestehender Datenbankeinträge ------------------------------
# -----------------------------------------------------------------------------
function EDITmodifyaa () {
    local database=$1            # Datenbank
    local dir_database=$(dirname $1) # Verzeichnis der Datenbank
    local dir_tmp=$2             # Verzeichnis für temporäre Dateien
    local dir_modules=$3         # Verzeichnis mit Modulen (awk-Skript)
    local table=$4               # Tabelle
    local reference="$5"         # Referenz
    local modify=$6              # modifizieren: true/false
    local reset="$7"             # true:  Zurücksetzen mit Deaktivierung
                                 # false: nur Korrigieren; erhalten des Lernst.
                                 # sonst: Zurücksetzen ohne Deaktivierung

    if ! check_table ${database} "${table}"; then exit 1; fi
    if [[ ! ${reference} =~ ^[0-9]+$ ]]; then
        printf "${err} ${reference} ${EDIT_err_noint}\n"
        return 1
    elif [ ${reference} -gt $(cat "${dir_dtb}/${table:0:4}/${table}.txt"      \
         | wc -l) ] || [ ${reference} -eq 0 ]; then
        printf "${err} ${reference} ${EDIT_err_noint}\n"
        return 1
    fi

    local aa=$(sqlite3 ${database}                                            \
        "SELECT aa          FROM ${table} WHERE reference=${reference}")
    local aa_remark=$(sqlite3 ${database}                                     \
        "SELECT aa_remark   FROM ${table} WHERE reference=${reference}")
    local aa_quantity=$(sqlite3 ${database}                                   \
        "SELECT aa_quantity FROM ${table} WHERE reference=${reference}")
    local aa_level=$(sqlite3 ${database}                                      \
        "SELECT aa_level    FROM ${table} WHERE reference=${reference}")
    local aa_level_old=$aa_level
    local BB=$(sqlite3 ${database}                                            \
        "SELECT BB          FROM ${table} WHERE reference=${reference}")
    local BB_remark=$(sqlite3 ${database}                                     \
        "SELECT BB_remark   FROM ${table} WHERE reference=${reference}")
    local BB_quantity=$(sqlite3 ${database}                                   \
        "SELECT BB_quantity FROM ${table} WHERE reference=${reference}")
    local BB_level=$(sqlite3 ${database}                                      \
        "SELECT BB_level    FROM ${table} WHERE reference=${reference}")
    local BB_level_old=$BB_level

    if [ "${reset}" == "false" ]; then
        local aa_highest=$(sqlite3 ${database}                                \
            "SELECT aa_highest  FROM ${table} WHERE reference=${reference}")
        local aa_first=$(sqlite3 ${database}                                  \
            "SELECT aa_first    FROM ${table} WHERE reference=${reference}")
        local aa_last=$(sqlite3 ${database}                                   \
            "SELECT aa_last     FROM ${table} WHERE reference=${reference}")
        local aa_next=$(sqlite3 ${database}                                   \
            "SELECT aa_next     FROM ${table} WHERE reference=${reference}")

        local BB_highest=$(sqlite3 ${database}                                \
            "SELECT BB_highest  FROM ${table} WHERE reference=${reference}")
        local BB_first=$(sqlite3 ${database}                                  \
            "SELECT BB_first    FROM ${table} WHERE reference=${reference}")
        local BB_last=$(sqlite3 ${database}                                   \
            "SELECT BB_last     FROM ${table} WHERE reference=${reference}")
        local BB_next=$(sqlite3 ${database}                                   \
            "SELECT BB_next     FROM ${table} WHERE reference=${reference}")
    elif [ "${reset}" == "true" ]; then
        local aa_level='-1'
        local aa_highest='-1'
        local aa_first='0'
        local aa_last='0'
        local aa_next='0'
        local BB_level='-1'
        local BB_highest='-1'
        local BB_first='0'
        local BB_last='0'
        local BB_next='0'
    else
        local aa_level='0'
        local aa_highest='0'
        local aa_first='0'
        local aa_last='0'
        local aa_next='0'
        local BB_level='0'
        local BB_highest='0'
        local BB_first='0'
        local BB_last='0'
        local BB_next='0'
    fi

    if [ "${modify}" == "true" ]; then
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}${table:0:2}:${clr_normal}"
        read -i "${aa}"        -p " " -e aa
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}():${clr_normal}"
        read -i "${aa_remark}" -p " " -e aa_remark
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}##:${clr_normal}"
        read -i ""             -p " " -e aa_quantity
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}lv:${clr_normal}"
        read -i "${aa_level}"  -p " " -e aa_level

        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}${table:2:2}:${clr_normal}"
        read -i "${BB}"        -p " " -e BB
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}():${clr_normal}"
        read -i "${BB_remark}" -p " " -e BB_remark
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}##:${clr_normal}"
        read -i ""             -p " " -e BB_quantity
        printf "${clr_red}${edit}${clr_normal} "
        printf "${clr_bold}lv:${clr_normal}"
        read -i "${BB_level}"  -p " " -e BB_level
    fi
    printf "\n"


    if [ "${aa_remark}"   ]; then local aa_rem=" (${aa_remark})"     ; fi
    if [ "${BB_remark}"   ]; then local BB_rem=" (${BB_remark})"     ; fi
    if [ "${aa_quantity}" ]; then local aa_quantity="@${aa_quantity}"; fi
    if [ "${BB_quantity}" ]; then
        if [ "${new_aa_quantity}" ]; then
            local BB_quantity="@@${BB_quantity}"
        else
            local BB_quantity="@${BB_quantity}"
        fi
    fi


    EDITdelete  "${database}"                                                 \
                "${dir_tmp}"                                                  \
                "${dir_modules}"                                              \
                "true"                                                        \
                "${table}" <<<                                                \
                    $(sed -n "s/\t/@/g;${reference}p"                         \
                    "${dir_dtb}/${table:0:4}/${table}.txt")

    EDITadd     "${database}"                                                 \
                "${dir_tmp}"                                                  \
                "${dir_modules}"                                              \
                "true"                                                        \
                "false"                                                       \
                "${table}" <<<                                                \
                    "${aa}${aa_rem}@${BB}${BB_rem}${aa_quantity}${BB_quantity}"

    function setsql () {
        local setarg=$1

        baa=$(echo ${aa} | sed "s/'/''/g")
        baa_remark=$(echo ${aa_remark} | sed "s/'/''/g")
        bBB=$(echo ${BB} | sed "s/'/''/g")
        bBB_remark=$(echo ${BB_remark} | sed "s/'/''/g")

        sqlite3 ${database} -noheader "
            UPDATE
                ${table}
            SET
                ${setarg}
            WHERE
                aa='${baa}'
                AND aa_remark='${baa_remark}'
                AND BB='${bBB}'
                AND BB_remark='${bBB_remark}'
            "
    }


if [ ! $reset ]; then
    if ! isint $aa_level; then
        local aa_level=$aa_level_old
    elif [ $aa_level -gt $levmax ]; then
        local aa_level=${levmax}
    fi

    if ! isint $BB_level; then
        local BB_level=$BB_level_old
    elif [ $BB_level -gt $levmax ]; then
        local BB_level=${levmax}
    fi
fi

    if   [ $aa_level -lt 0 ] && [ $BB_level -ge 0 ]; then
        aa_level=$aa_level_old
        BB_level=$BB_level_old
    elif [ $BB_level -lt 0 ] && [ $aa_level -ge 0 ]; then
        aa_level=$aa_level_old
        BB_level=$BB_level_old
    fi


    setsql "aa_level='${aa_level}'"
    setsql "aa_highest='${aa_highest}'"
    setsql "aa_first='${aa_first}'"
    setsql "aa_last='${aa_last}'"
    setsql "aa_next='${aa_next}'"
    setsql "BB_level='${BB_level}'"
    setsql "BB_highest='${BB_highest}'"
    setsql "BB_first='${BB_first}'"
    setsql "BB_last='${BB_last}'"
    setsql "BB_next='${BB_next}'"
}
# <<< -------------------------------------------------------------------------
# -----------------------------------------------------------------------------

#!/usr/bin/awk -f

# -----------------------------------------------------------------------------
# --- createdb.awk                                                          ---
# ---                                                                       ---
# --- Das Programm erzeugt aus Textdateien mit Vokabellisten ein            ---
# --- geeignetes Format für die von vgme.sh benötigte sqlite3-Datenbank.    ---
# -----------------------------------------------------------------------------


BEGIN { FS="\t"  }
  { OFS="@";
    gsub(")","",$0);             # entferne alle ")"
    split($1,aa," [(]");         # teile 1. bzw. 2. "Wort" bei " (" in Unter-
    split($2,BB," [(]");         # worte und speichere in Array x[*], y[*]

    # Falls die Wortanzahl nicht explizit gegeben ist (Spalte 3, bzw. 4 leer)
    # wird die Summe (split) der durch , oder ; getrennten Wörter verwendet.
    aa_quantity = $3 !~ /^$/ ? $3 : split(aa[1],z,"[,;]");
    BB_quantity = $4 !~ /^$/ ? $4 : split(BB[1],z,"[,;]");

    print NR         ,           # reference
          aa[1]      ,           # aa
          aa[2]      ,           # aa_remark
          aa_quantity,           # aa_quantity
          -1         ,           # aa_level
          -1         ,           # aa_highest
           0         ,           # aa_first
           0         ,           # aa_last
           0         ,           # aa_next
          BB[1]      ,           # BB
          BB[2]      ,           # BB_remark
          BB_quantity,           # BB_quantity
          -1         ,           # BB_level
          -1         ,           # BB_highest
           0         ,           # BB_first
           0         ,           # BB_last
           0         ;           # BB_next
  }
